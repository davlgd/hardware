# Copyright 2012-2016 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require bash-completion gsettings gtk-icon-cache freedesktop-desktop

SUMMARY="Tools for the Hughski ColorHug open-source colorimeter"
DESCRIPTION="
The Hughski ColorHug colorimeter is a low cost open-source hardware sensor used to calibrate
screens.

This package includes the client tools which allows the user to upgrade the firmware on the sensor
or to access the sensor from command line scripts.
"
HOMEPAGE="http://www.hughski.com"
DOWNLOADS="https://people.freedesktop.org/~hughsient/releases/${PNV}.tar.xz"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="gobject-introspection"

# Wants to access USB device if plugged in
RESTRICT="test"

DEPENDENCIES="
    build:
        dev-util/intltool[>=0.50.0]
        sys-devel/gettext[>=0.17]
        virtual/pkg-config[>=0.16]
    build+run:
        dev-libs/glib:2[>=2.31.10][gobject-introspection(+)?]
        dev-libs/libgusb[>=0.2.2]
        dev-util/itstool
        gnome-desktop/colord-gtk[>=0.1.24]
        gnome-desktop/libsoup:2.4
        media-libs/lcms2
        media-libs/libcanberra[>=0.10][providers:gtk3]
        sys-apps/colord[>=1.2.9]
        x11-libs/gtk+:3[>=3.11.2][gobject-introspection?]
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=0.9.8] )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-nls
    --disable-bash-completion
    --disable-static
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( 'gobject-introspection introspection' )

src_install() {
    gsettings_src_install

    dobashcompletion data/bash/colorhug-cmd colorhug-cmd
}

pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gsettings_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gsettings_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

