# Copyright 2009 Ingmar Vanhassel
# Copyright 2010-2017 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=libimobiledevice tag=${PV} ]
require flag-o-matic
require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 ] ]

# Still no proper docs for multi-python so if someone cares, write some.
require python [ blacklist=none with_opt=true has_lib=true multiunpack=true ]

SUMMARY="library to handle Apple Property Lists whether they are binary or XML"

HOMEPAGE="https://www.libimobiledevice.org ${HOMEPAGE}"

LICENCES="GPL-2 LGPL-2.1"
SLOT="2.0"
MYOPTIONS="python"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        python? ( dev-python/Cython[>=0.17.0] )
        !${CATEGORY}/${PN}:0 [[
            description = [ 2.0 slot move ]
            resolution = uninstall-blocked-before
        ]]
"

# Multiple job support is currently broken.
DEFAULT_SRC_COMPILE_PARAMS=( -j1 )

unpack_one_multibuild() {
    if ever at_least scm; then
        scm_src_unpack
    else
        default
    fi
}

prepare_one_multibuild() {
    autotools_src_prepare
}

configure_one_multibuild() {
    # TODO (compnerd) building with LTO results in a miscompilation (bug ???)
    # plist_empty_cmp test indicates the miscompilation; need to work out the wrong transformation
    # reproducible with both BFD ld as well as gold - potential middle-end bug?
    filter-flags -flto

    local myconf=( --without-sanitizers --without-fuzzers )

    option python || myconf+=( --without-cython )

    econf "${myconf[@]}"
}

test_one_multibuild() {
    esandbox disable
    emake -j1 check
    esandbox enable
}

install_one_multibuild() {
    default

    insinto /usr/$(exhost --target)/include/plist/cython
    doins cython/plist.pxd
}

