# Copyright 2019 Danilo Spinella <danyspin97@protonmail.com>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'libreoffice-6.3.2.2.exheres-0', which is:
#   Copyright 2008-2011, 2014 Ingmar Vanhassel <ingmar@exherbo.org>
#   Copyright 2011-2013 Benedikt Morbach <moben@exherbo.org>
#   Copyright 2011 Dimitry Ishenko <dimitry.ishenko@gmail.com>

MY_PV=${PV/_p/p}

require github [ user='nmeum' suffix=tar.xz release=${MY_PV} ] \
    cmake [ ninja=true ] \
    bash-completion

SUMMARY="Android command line utilities"

LICENCES="Apache-2.0 GPL-2 ISC MIT"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    ( providers: eudev systemd ) [[ number-selected = exactly-one ]]
"

# TODO: re-add once 1.0.28 has been released:
#     dev-libs/libusb:1[>=1.0.28]
DEPENDENCIES="
    build:
        dev-cpp/gtest   [[ note = [ Included in libziparchive's zip_writer.h ] ]]
        dev-lang/go[>=1.13]
        dev-lang/perl:*
        dev-libs/libunwind
        virtual/pkg-config
    build+run:
        app-arch/brotli
        app-arch/lz4
        app-arch/zstd
        dev-libs/fmt
        dev-libs/pcre2
        dev-libs/protobuf:=
        sys-libs/zlib
        providers:eudev? ( sys-apps/eudev )
        providers:systemd? ( sys-apps/systemd )
        !dev-util/adb [[
            description = [ adb is provided by this package ]
            resolution = uninstall-blocked-after
        ]]
        !dev-util/fastboot [[
            description = [ fastboot is provided by this package ]
            resolution = uninstall-blocked-after
        ]]
    test:
        dev-libs/libunwind
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    # can be removed once libusb has been re-added (ANDROID_TOOLS_LIBUSB_ENABLE_UDEV)
    -DANDROID_TOOLS_LIBUSB_ENABLE_UDEV:BOOL=TRUE
    -DANDROID_TOOLS_USE_BUNDLED_FMT:BOOL=FALSE
    # requires dev-libs/libusb:1[>=1.0.28]
    -DANDROID_TOOLS_USE_BUNDLED_LIBUSB:BOOL=TRUE
)

src_unpack() {
    cmake_src_unpack

    edo pushd "${CMAKE_SOURCE}"/vendor/boringssl/crypto
    esandbox disable_net
    edo go mod download
    esandbox enable_net
    edo popd
}

src_prepare() {
    cmake_src_prepare

    # disable automatic installation
    edo sed \
        -e '/BASH_COMPLETION_DIR/d' \
        -e '/LICENSE_DIR/d' \
        -i vendor/CMakeLists.txt
}

src_install() {
    cmake_src_install

    dobashcompletion "${CMAKE_SOURCE}"/vendor/adb/adb.bash adb
    dobashcompletion "${CMAKE_SOURCE}"/vendor/core/fastboot/fastboot.bash fastboot
}

