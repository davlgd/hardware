# Copyright 2019-2020 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require nvidia-cuda-toolkit [ driver_version=387.26 gcc_slot=6 ]

DOWNLOADS="
    listed-only:
        https://developer.nvidia.com/compute/cuda/$(ever range 1-2)/Prod/local_installers/cuda_${PV}_${DRIVER_VERSION}_linux ->
            cuda_${PV}_${DRIVER_VERSION}_linux.run
"

PLATFORMS="~amd64"

src_unpack() {
    nvidia-cuda-toolkit_src_unpack

    local toolkit_root="${WORK}/builds/cuda-toolkit"
    edo mkdir -p "${WORK}/builds/cuda-toolkit"
    edo sh "${WORK}"/run_files/cuda-linux.${PV}-*.run --target "${toolkit_root}" --noexec
    if option examples; then
        edo sh "${WORK}"/run_files/cuda-samples.${PV}-*.run --target "${toolkit_root}" --noexec
    fi
}

src_prepare() {
    nvidia-cuda-toolkit_src_prepare

    edo pushd builds/cuda-toolkit
    # We'll override floatn.h from glibc only for cuda
    edo mkdir include/bits
    edo cp /usr/host/include/bits/floatn.h include/bits/
    expatch -p0 "${FILES}"/cudacc-floatn.patch
    edo popd
}

