# Copyright 2019-2022 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

myexparam driver_version
myexparam gcc_slot

exparam -v DRIVER_VERSION driver_version
exparam -v GCC_SLOT gcc_slot

export_exlib_phases pkg_setup src_unpack src_prepare src_install

SUMMARY="NVIDIA CUDA Toolkit"
DESCRIPTION="
The NVIDIA CUDA Toolkit provides a development environment for creating high performance
GPU-accelerated applications. With the CUDA Toolkit, you can develop, optimize and deploy your
applications on GPU-accelerated embedded systems, desktop workstations, enterprise data centers,
cloud-based platforms and HPC supercomputers. The toolkit includes GPU-accelerated libraries,
debugging and optimization tools, a C/C++ compiler and a runtime library to deploy your
application.
"
HOMEPAGE="https://developer.nvidia.com/cuda-toolkit"
if ever at_least 11.8.0; then
    DOWNLOADS="
        listed-only:
            https://developer.download.nvidia.com/compute/cuda/$(ever range 1-3)/local_installers/cuda_${PV}_${DRIVER_VERSION}_linux.run
    "
else
    DOWNLOADS="
        listed-only:
            https://developer.download.nvidia.com/compute/cuda/$(ever range 1-2)/Prod/local_installers/cuda_${PV}_${DRIVER_VERSION}_linux.run
    "
fi

LICENCES="NVIDIA-CUDA"
SLOT="0"
MYOPTIONS="examples"

RESTRICT="fetch strip test"

DEPENDENCIES="
    build+run:
        sys-devel/gcc:${GCC_SLOT} [[ note = [ latest GCC version supported by CUDA ] ]]
    run:
        dev-libs/libxml2:2.0
        x11-drivers/nvidia-drivers[>=${DRIVER_VERSION}]
        examples? (
            x11-dri/freeglut
            x11-dri/glu
            x11-libs/libX11
            x11-libs/libXext
        )
"

nvidia-cuda-toolkit_pkg_setup() {
    exdirectory --allow /opt
}

nvidia-cuda-toolkit_src_unpack() {
    edo sh "${FETCHEDDIR}"/cuda_${PV}_${DRIVER_VERSION}_linux.run --target "${WORK}" --noexec
}

nvidia-cuda-toolkit_src_prepare () {
    default

    if ! ever at_least 11.8.0; then
        edo pushd builds/cuda-toolkit

        # debugger
        edo rm -rf bin/cuda-gdb{,server} extras/{Debugger,cuda-gdb-${PV}.src.tar.gz} share
        # eclipse
        edo rm -rf bin/{nsight,nsight_ee_plugins_manage.sh,nvvp,computeprof} libnvvp libnsight nsightee_plugins nsight-{compute-2019.5.0,systems-2019.5.2}
        # examples/demos
        edo rm -rf nvml {nvvm,nvvmx}/libnvvm-samples
        # extras
        edo rm -rf extras/Sanitizer src tools
        # CUPTI samples
        edo rm -rf extras/CUPTI/sample*

        if ! option examples ; then
            # demo_suite
            edo rm -rf extras
        fi

        edo popd
    fi
}

nvidia-cuda-toolkit_src_install() {
    dodir /usr/share/doc/${PNVR}

    if ever at_least 11.8.0; then
        edo pushd builds

        insinto /opt/cuda

        local installbindirs=(
            cuda_cuobjdump
            cuda_cuxxfilt
            cuda_nvcc
            cuda_nvdisasm
            cuda_nvprof
            cuda_nvprune
        )

        local installtargetsdirs=(
            cuda_cccl
            cuda_cudart
            cuda_nvcc
            cuda_nvprof
            cuda_nvml_dev
            cuda_nvrtc
            cuda_nvtx
            cuda_profiler_api
            libcublas
            libcufile
            libcufft
            libcurand
            libcusolver
            libcusparse
            libnpp
            libnvjpeg
        )

        local installextrasdirs=(
            cuda_cupti
        )

        if option examples ; then
            installextrasdirs+=(
                cuda_demo_suite
            )
        fi

        if ever at_least 12.0.0; then
            installtargetsdirs+=(
                libnvjitlink
            )
        else
            installbindirs+=(
                cuda_memcheck
            )
        fi

        for directory in "${installbindirs[@]}"; do
            doins -r ${directory}/bin
        done

        for directory in "${installtargetsdirs[@]}"; do
            doins -r ${directory}/targets
        done

        for directory in "${installextrasdirs[@]}"; do
            doins -r ${directory}/extras
        done

        # symlinks
        dosym targets/x86_64-linux/include /opt/cuda/include
        dosym targets/x86_64-linux/lib /opt/cuda/lib64

        # nvvm/nvvmx
        edo rm -rf cuda_nvcc/nvvm/libnvvm-samples
        doins -r cuda_nvcc/nvvm
        dosym nvvm /opt/cuda/nvvmx

        # install misc
        doins EULA.txt
        # additional CUPTI docs
        if ! ever at_least 12.6.0 ; then
            dodir /usr/share/doc/${PNVR}/pdf
            edo mv cuda_cupti/extras/CUPTI/doc/pdf/Cupti.pdf "${IMAGE}"/usr/share/doc/${PNVR}/pdf/
            edo rmdir cuda_cupti/extras/CUPTI/doc/pdf
        fi
        dodir /usr/share/doc/${PNVR}/html
        edo mv cuda_cupti/extras/CUPTI/doc/* "${IMAGE}"/usr/share/doc/${PNVR}/html
        edo rm -rf cuda_cupti/extras/CUPTI/doc
        edo rm -rf "${IMAGE}"/opt/cuda/extras/CUPTI/{doc,samples}

        # nvidia_fs kernel module
        insinto /usr/src
        doins -r nvidia_fs/usr/src/*

        edo popd

        # remove empty directories
        edo find "${IMAGE}" -type d -empty -delete
    else
        # install man-pages
        edo mv builds/cuda-toolkit/doc/man "${IMAGE}"/usr/share
        if ever at_least 10.2.89; then
            # uuid.3 is provided by sys-apps/util-linux
            edo rm "${IMAGE}"/usr/share/man/man3/uuid.3
        fi
        # install docs
        edo mv builds/cuda-toolkit/doc/* "${IMAGE}"/usr/share/doc/${PNVR}/
        if ever at_least 10.2.89; then
            edo mv builds/cuda-toolkit/extras/CUPTI/doc/pdf/Cupti.pdf "${IMAGE}"/usr/share/doc/${PNVR}/pdf
            edo rmdir builds/cuda-toolkit/extras/CUPTI/doc/pdf
            edo mv builds/cuda-toolkit/extras/CUPTI/doc/Cupti/* "${IMAGE}"/usr/share/doc/${PNVR}/html/cupti
            edo rm -rf builds/cuda-toolkit/extras/CUPTI/doc
        fi

        insinto /opt/cuda
        doins -r builds/cuda-toolkit/*
    fi

    local bindirs=( "${IMAGE}"/opt/cuda/{bin,nvvm/bin} )
    if ever at_least 10.2.89; then
        bindirs+=( "${IMAGE}"/opt/cuda/nvvmx/bin )
    fi
    if option examples ; then
        bindirs+=( "${IMAGE}"/opt/cuda/extras/demo_suite )
    fi
    for file in $(find "${bindirs[@]}" -maxdepth 1 -type f); do
        edo chmod 0755 ${file}
    done

    # latest GCC version supported by CUDA
    dosym /usr/$(exhost --target)/bin/gcc-${GCC_SLOT} opt/cuda/bin/gcc
    dosym /usr/$(exhost --target)/bin/g++-${GCC_SLOT} opt/cuda/bin/g++

    insinto /etc/env.d
    hereins 99cuda <<EOF
PATH=/opt/cuda/bin
CUDA_PATH=/opt/cuda
LDPATH=/opt/cuda/lib64:/opt/cuda/nvvm/lib64:/opt/cuda/extras/CUPTI/lib64
EOF
}

