# Copyright 2016 Marc-Antoine Perennou
# Distributed under the terms of the GNU General Public License v2

require utf8-locale

if ever is_scm ; then
    SCM_REPOSITORY="https://git.kernel.org/pub/scm/linux/kernel/git/firmware/${PN}.git"
    SCM_BRANCH=main
    require scm-git
else
    DOWNLOADS="mirror://kernel/linux/kernel/firmware/${PNV}.tar.xz"
fi

export_exlib_phases src_install src_test pkg_preinst

SUMMARY="Firmware blobs for use with the Linux kernel"
HOMEPAGE="https://git.kernel.org/pub/scm/linux/kernel/git/firmware/${PN}.git"

LICENCES="
GPL-3 Abilis IntcSST2 Marvell OLPC adsp_sst agere atheros_firmware broadcom_bcm43xx ca0132
cavium chelsio_firmware cw1200 e100 ene_firmware fw_sst_0f28 go7007 i2400m ibt_firmware it913x
iwlwifi_firmware kaweth moxa mwl8335 myri10ge_firmware nvidia open-ath9k-htc-firmware phanfw
qat_firmware qla1280 qla2xxx r8a779x_usb3 ralink-firmware ralink_a_mediatek_company_firmware
rtlwifi_firmware siano tda7706-firmware ti-connectivity ti-keystone ueagle-atm4-firmware
via_vt6656 wl1251 xc4000 xc5000 xc5000c
"
SLOT="0"
MYOPTIONS=""

RESTRICT="strip test"

DEPENDENCIES="
    build:
        app-misc/rdfind
    build+run: (
        !firmware/htc_9271
        !firmware/i915-firmware
        !firmware/intel-bluetooth
        !firmware/radeon-ucode
        !firmware/rt73
        !firmware/rtl_nic
        !net-wireless/ar3k-firmware
        !net-wireless/iwlwifi-100-ucode
        !net-wireless/iwlwifi-1000-ucode
        !net-wireless/iwlwifi-105-ucode
        !net-wireless/iwlwifi-135-ucode
        !net-wireless/iwlwifi-2000-ucode
        !net-wireless/iwlwifi-2030-ucode
        !net-wireless/iwlwifi-3160-ucode
        !net-wireless/iwlwifi-3945-ucode
        !net-wireless/iwlwifi-4965-ucode
        !net-wireless/iwlwifi-5000-ucode
        !net-wireless/iwlwifi-5150-ucode
        !net-wireless/iwlwifi-6000-ucode
        !net-wireless/iwlwifi-6000g2a-ucode
        !net-wireless/iwlwifi-6000g2b-ucode
        !net-wireless/iwlwifi-6050-ucode
        !net-wireless/iwlwifi-7260-ucode
        !net-wireless/iwlwifi-7265-ucode
        !net-wireless/iwlwifi-8000-ucode
        !net-wireless/rtl8192cu-firmware
        !net-wireless/rtl8192se
        !sys-apps/amd-ucode
    ) [[
        *description = [ linux-firmware already contains this firmware ]
        *resolution = uninstall-blocked-before
    ]]
"

linux-firmware_src_install() {
    emake -j1 DESTDIR="${IMAGE}" FIRMWAREDIR="/usr/$(exhost --target)/lib/firmware" install
    emake -j1 DESTDIR="${IMAGE}" FIRMWAREDIR="/usr/$(exhost --target)/lib/firmware" dedup
}

linux-firmware_src_test() {
    require_utf8_locale
    default
}

linux-firmware_pkg_preinst() {
    # make update from <20220913 work (failure replacing a directory with a symlink)
    if has_version 'firmware/linux-firmware[<20220913]' ; then
        nonfatal edo rm -rf "${ROOT}"/usr/$(exhost --target)/lib/firmware/qcom/LENOVO/21BX
    fi
}

