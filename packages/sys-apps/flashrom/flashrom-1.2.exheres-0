# Copyright 2013-2016 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

MY_PNV=${PN}-v${PV}

require udev-rules [ udev_files=[ util/z60_flashrom.rules ] ]

SUMMARY="Utility for identifying, reading, writing, verifying and erasing flash chips"
DESCRIPTION="
flashrom is designed to flash BIOS/EFI/coreboot/firmware/optionROM images on
mainboards, network/graphics/storage controller cards, and various other programmer
devices.
"
HOMEPAGE="https://flashrom.org"
DOWNLOADS="https://download.flashrom.org/releases/${MY_PNV}.tar.bz2"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        dev-libs/libftdi:=
        dev-libs/libusb:1
        sys-apps/pciutils
    suggestion:
        sys-apps/dmidecode
"

DEFAULT_SRC_COMPILE_PARAMS=( WARNERROR=no )

DEFAULT_SRC_INSTALL_PARAMS=(
    PREFIX=/usr
    MANDIR=/usr/share/man
    RANLIB=$(exhost --tool-prefix)ranlib
    PKG_CONFIG_LIBDIR=/usr/$(exhost --target)/lib/pkgconfig
)

WORK=${WORKBASE}/${MY_PNV}

src_prepare() {
    default

    edo sed -i -e "s:sbin:$(exhost --target)/bin:g" Makefile
}

src_compile() {
    default

    edo pushd util/ich_descriptors_tool
    default
    edo popd
}

src_install() {
    default

    install_udev_files

    dodoc Documentation/{mysteries_intel,serprog-protocol}.txt

    pushd util/ich_descriptors_tool
    exeinto /usr/$(exhost --target)/bin
    doexe ich_descriptors_tool
    popd
}

