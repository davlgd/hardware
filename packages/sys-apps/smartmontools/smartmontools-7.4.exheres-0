# Copyright 2009 Sterling X. Winter <replica@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'smartmontools-5.38.ebuild' from Gentoo, which is:
#     Copyright 1999-2008 Gentoo Foundation

require flag-o-matic systemd-service sourceforge [ suffix=tar.gz ] \
    option-renames [ renames=[ 'libcap-ng caps' ] ] \
    openrc-service [ openrc_confd_files=[ "${FILES}/openrc/confd" ] ]

SUMMARY="Utilities for SMART-capable storage devices"
DESCRIPTION="
The smartmontools package contains two utility programs (smartctl and smartd)
to control and monitor storage systems using the Self-Monitoring, Analysis and
Reporting Technology (SMART) system built into most modern ATA and SCSI hard
disks. In many cases, these utilities will provide advanced warning of disk
degradation and failure.
"
HOMEPAGE+=" https://www.${PN}.org"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~armv8 ~x86"
MYOPTIONS="
    caps [[ description = [ Use libcap-ng for capabilities support ] ]]
    static
    systemd [[ description = [ Support systemd status notification in smartd ] ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        caps? ( sys-libs/libcap-ng )
        systemd? ( sys-apps/systemd )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --localstatedir=/var
    --sbindir=/usr/$(exhost --target)/bin
    --disable-sample
    --with-attributelog=/var/lib/smartmontools/attrlog.
    --with-drivedbdir=/usr/share/smartmontools
    --with-savestates=/var/lib/smartmontools/smartd.
    --with-update-smart-drivedb
    --with-nvme-devicescan
    --without-initscriptdir
    --without-selinux
)
DEFAULT_SRC_CONFIGURE_OPTIONS=(
    "systemd --with-systemdsystemunitdir=${SYSTEMDSYSTEMUNITDIR}"
    "systemd --with-systemdenvfile=/etc/smartmontools"
)
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    'caps libcap-ng'
    'systemd libsystemd'
)

src_compile() {
    option static && append-ldflags -static

    default
}

src_install() {
    default

    install_openrc_files
    keepdir /var/lib/${PN}
    keepdir /etc/smartd_warning.d
}

