# Copyright 2021-2024 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=fwupd release=${PV} suffix=tar.xz ] \
    python [ blacklist=2 multibuild=false ] \
    meson

SUMMARY="EFI executable for fwupd"
HOMEPAGE+=" https://www.fwupd.org"

LICENCES="LGPL-2.1"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

# automagic on unpackaged python-uswid
DEPENDENCIES="
    build:
        dev-python/pefile[python_abis:*(-)?]
        sys-boot/gnu-efi[>=3.0.18]
        sys-devel/binutils
"

MESON_SRC_CONFIGURE_PARAMS=(
    -Defi-includedir=/usr/$(exhost --target)/include/efi
    -Defi-libdir=/usr/$(exhost --target)/lib
    -Defi_sbat_distro_id="exherbo"
)

src_prepare() {
    meson_src_prepare

    # respect selected python abi
    edo sed \
        -e "s:python3:python$(python_get_abi):g" \
        -i efi/generate_{binary,sbat}.py
}

