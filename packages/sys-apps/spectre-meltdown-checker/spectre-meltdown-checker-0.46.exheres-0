# Copyright 2018-2023 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=speed47 tag=v${PV} ]

SUMMARY="Spectre and Meltdown checker"
DESCRIPTION="
A shell script to tell if your system is vulnerable against the several \"speculative execution\"
CVEs that were made public since 2018.

* CVE-2017-5753 [bounds check bypass] aka 'Spectre Variant 1'
* CVE-2017-5715 [branch target injection] aka 'Spectre Variant 2'
* CVE-2017-5754 [rogue data cache load] aka 'Meltdown' aka 'Variant 3'
* CVE-2018-3640 [rogue system register read] aka 'Variant 3a'
* CVE-2018-3639 [speculative store bypass] aka 'Variant 4'
* CVE-2018-3615 [L1 terminal fault] aka 'Foreshadow (SGX)'
* CVE-2018-3620 [L1 terminal fault] aka 'Foreshadow-NG (OS)'
* CVE-2018-3646 [L1 terminal fault] aka 'Foreshadow-NG (VMM)'
* CVE-2018-12126 [microarchitectural store buffer data sampling (MSBDS)] aka 'Fallout'
* CVE-2018-12130 [microarchitectural fill buffer data sampling (MFBDS)] aka 'ZombieLoad'
* CVE-2018-12127 [microarchitectural load port data sampling (MLPDS)] aka 'RIDL'
* CVE-2019-11091 [microarchitectural data sampling uncacheable memory (MDSUM)] aka 'RIDL'
* CVE-2019-11135 [TSX asynchronous abort] aka 'TAA' aka 'ZombieLoad V2'
* CVE-2018-12207 [machine check exception on page size changes (MCEPSC)] aka 'No eXcuses' aka 'iTLB Multihit'
* CVE-2020-0543 [Special Register Buffer Data Sampling (SRBDS)]
* CVE-2023-20593 [Cross-Process Information Leak] aka 'Zenbleed'

Without options, it will inspect your currently running kernel. You can also specify a kernel image
on the command line, if you would like to inspect a kernel you're not running. The script will do
its best to detect mitigations, including backported non-vanilla patches, regardless of the
advertised kernel version number.
"

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES=""

src_test() {
    :
}

src_install() {
    newbin ${PN}.sh ${PN}

    emagicdocs
}

