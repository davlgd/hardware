# Copyright 2016 Marc-Antoine Perennou <keruspe@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

if ever is_scm; then
    SCM_REPOSITORY="https://git.kernel.org/pub/scm/network/wireless/iwd.git"
    SCM_ell_REPOSITORY="https://git.kernel.org/pub/scm/libs/ell/ell.git"
    SCM_ell_UNPACK_TO="${WORKBASE}/ell"
    SCM_SECONDARY_REPOSITORIES="ell"
    require scm-git autotools [ supported_autoconf=[ 2.7 ] supported_automake=[ 1.16 ] ]
    DOWNLOADS=""
else
    DOWNLOADS="mirror://kernel/linux/network/wireless/${PNV}.tar.xz"
fi

require systemd-service openrc-service

export_exlib_phases src_install

SUMMARY="Wireless daemon for Linux"
HOMEPAGE="https://git.kernel.org/cgit/network/wireless/${PN}.git/"

LICENCES="LGPL-2.1"
SLOT="0"
MYOPTIONS="
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        dev-python/docutils
        virtual/pkg-config
    build+run:
        sys-apps/dbus
        sys-libs/readline:=
    test:
        (
            providers:libressl? ( dev-libs/libressl:* )
            providers:openssl? ( dev-libs/openssl:* )
        ) [[ note = [ openssl command line tool ] ]]
"

# causes reliably something like
# sydbox: pink: syd_read_socket_address:253 failed for pid:165209
RESTRICT="test"

if ever at_least scm ; then
    :
else
    DEPENDENCIES+="
        build+run:
            dev-libs/ell[>=0.72]
    "

    DEFAULT_SRC_CONFIGURE_PARAMS+=( --enable-external-ell )
fi

DEFAULT_SRC_CONFIGURE_PARAMS+=(
    --localstatedir=/var
    --enable-debug
    --enable-optimization
    --enable-pie
    --enable-manual-pages
    --disable-gcov
    # Alternative to readline, fails to build though
    --disable-libedit
    --with-systemd-unitdir="${SYSTEMDSYSTEMUNITDIR}"
    --with-systemd-modloaddir="/usr/$(exhost --target)/lib/modules-load.d"
    --with-systemd-networkdir="/usr/$(exhost --target)/lib/systemd/network"
)

iwd_src_install() {
    default

    install_openrc_files
    keepdir /var/lib/${PN}
}

